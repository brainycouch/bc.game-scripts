let fullHistory = [];
let config = {};
let initialAmount = currency.amount;
let initialBetMultiplier = 3;
/** BET SETTINGS **/
config.betTitle = {
  label: `Set Your Bet - ${currency.amount} ${currency.currencyName} Available`,
  type: 'title'
};
config.betMultiplier = {
  label: 'Bet Multiplier',
  type: 'number',
  value: 1.25
};
config.minMultiplier = {
  label: 'Minimum Bet Multiplier',
  type: 'number',
  value: 2.2
};
config.bet = {
  label: 'bet',
  type: 'number',
  value: currency.minAmount * initialBetMultiplier
};
config.betMax = {
  label: 'Maximum Bet',
  type: 'number',
  value: currency.minAmount * initialBetMultiplier * config.minMultiplier.value
};
/** PAYOUT SETTINGS */
config.payoutTitle = {
  label: 'Choose Odds',
  type: 'title'
};
config.payout = {
  label: 'Payout Multiplier',
  type: 'number',
  value: 1.15
};
/** WIN SETTINGS */
config.winTitle = {
  label: 'When You Win - Reset Bet After # of Wins',
  type: 'title'
};
config.winBigBet = {
  label: 'Big Bet Every # of Wins',
  type: 'number',
  value: 3
}
config.winReset = {
  label: 'Reset to Base Bet every # of Wins',
  type: 'number',
  value: 7
}
/** LOSE SETTINGS */
config.loseTitle = {
  label: 'When You Lose - Pause After # of Loses',
  type: 'title'
};
config.lossReset = {
  label: '# of Loses',
  type: 'number',
  value: 1
}
/** PAUSE SETTINGS */
config.pauseTitle = {
  label: 'When You Lose - Pause After Loss Count',
  type: 'title'
};
config.pauseCount = {
  label: 'Length of Pause (seconds)',
  type: 'number',
  value: 10
}
/** RATE LIMIT SETTINGS */
config.rateLimitTitle = {
  label: 'How Often to Place Bets',
  type: 'title'
};
config.rateLimit = {
  label: 'Delay (not sure if exactly time or what, but timeout 1500 times below)',
  type: 'number',
  value: 0.2
}

(() => {
  const formatify = o => JSON.stringify(o)
      .replace(/([{,\[])/g, "$1\r\n\t")
      .replace(/(\}|\])/g, "\r\n$1");
  JSON.formatify = formatify;
  console.formatify = () => console.debug(
    JSON.formatify(o)
  );
})();

/** CUSTOM LOGGING */
console.formatify({ config });
/*******************/

const tramp = (fun, ...args) => {
  let result = fun.apply(fun, ...args);
  while (typeof result == "function") {
    result = result();
  }
  return result;
}

game.betQueue = []; // holder for timeout ids
game.totalProfits = 0;
game.balance = initialAmount;
game.updateBalance = curr => curr.minAmount;

/** GAME START */
let main = () => {

  /** METRICS */
  let lossCounter = 0;
  let winCounter = 0;

  /** MEMBER VARS */
  let nextBet = config.bet.value;
  let lastGameWin = false;
  let totalProfits = 0;
  let bigBet = false;
  let payout = config.payout.value;
  let resultMultiplier = 0;

  /** STOP HANDLER */
  let hardStop = false;
  let pause = false;
  let pauseCountDown = config.pauseCount.value;

  // override game.stop()
  let orgStop = game.stop;
  game.stop = () => {
    hardStop = true;
    log.error('STOPPING');
    game.clearBets();
    orgStop.apply(game);
  }

  game.clearBets = () => {
    for(let id in game.betQueue) {
      clearTimeout(id);
    }
  }

  /** BET HANDLER */
  game.onBet = () => {
    const trampBet = tramp(async ()=>{
      try {
        if (hardStop || pause) return;
        let profitOrDebit = nextBet * payout;
        if (
          // (currency.amount * 0.075 < (nextBet * payout)) ||
          (nextBet > config.betMax.value) ||
          (currency.amount < (nextBet * payout))
        ) {
          log.error("Profit not obtainable. Reset bet to base.");
          payout = 1.0102; // config.payout.value;
          nextBet = currency.minAmount * payout;
          resultMultiplier = config.betMultiplier.value;
        }
        nextBet = nextBet < currency.minAmount ? currency.minAmount : nextBet;
        nextBet = nextBet > config.betMax.value ? config.bet.value : nextBet;
        // RESULT VARS
        log.info(`Performing ${bigBet ? 'BIG ' : ''}Bet @ ${nextBet}`);
        if (!pause) {
          try {
            resultMultiplier = await game.bet(nextBet, payout); 
          } catch(e) {
            log.error(`ERROR: ${e.message}`);
            resultMultiplier = 1.0102;
          }
        }
        let resultPayout = resultMultiplier * nextBet;
        // RESULT HANDLER
        if (resultMultiplier > 1) {
          // WON
          totalProfits += profitOrDebit;
          // if (nextBet == currency.minAmount) nextBet = config.bet.value;
          winCounter++;
          if (lossCounter > 0 && (winCounter % (config.winBigBet.value-1)) == 0) lossCounter--;
          log.success(`WON:  ${resultPayout}!`);
          if ((winCounter % config.winReset.value) == 0) {
            resultMultiplier = config.betMultiplier.value;
            resultPayout = nextBet * resultMultiplier;

            log.info(`Reset Big Bet to ${resultMultiplier}`);
            // log.info(`Sleeping for ${config.pauseCount.value} rounds.`);
            // pause = true;
          }
          if ((winCounter % config.winBigBet.value) == 0) {
            nextBet = (resultMultiplier * config.minMultiplier.value * parseFloat(`1.${winCounter}`)) * nextBet;
            bigBet = true;
          } else {
            nextBet = nextBet * (resultMultiplier * parseFloat(`0.6${(winCounter == 0 ? 1 : winCounter)}`));
            bigBet = false;
          }
        } else {
          // LOST
          lossCounter++;
          winCounter = (winCounter/2).toFixed(0);
          totalProfits -= profitOrDebit
          log.error(`LOST: ${profitOrDebit}!!!`);
          resultMultiplier = config.betMultiplier.value;
          resultPayout = config.bet.value * resultMultiplier;
          nextBet = currency.minAmount;
          if (lossCounter >= config.lossReset.value || bigBet) {
            // game.stop();
            pause = true;
            game.clearBets();
            winCounter = 0;
            bigBet = false;
            log.info(`Pausing for ${config.pauseCount.value} seconds`);
            const count = time => {
              setTimeout(() => {
                if (time > 1) {
                  log.info(`Pausing for ${--time} seconds`);
                  count(time);
                }
              }, 1000);
            };
            count(config.pauseCount.value);
            setTimeout(() => {
              /* SAME AS BELOW */
              let balance = currency.amount + totalProfits;
              let losses = totalProfits < 0;
              log[losses ? "error" : "success"](`${losses ? "Losses" : "Profits"}: ${totalProfits}`);
              log[balance < currency.amount ? "error" : "success"](`Balance: ${balance}`);
              /**********/
              pause = false;
              game.bet(currency.minAmount, config.payout.value);
            }, 1000 * config.pauseCount.value);
          }
        }

        if (!pause) { // SAME AS ABOVE
          let balance = currency.amount + totalProfits;
          let losses = totalProfits < 0;
          log[losses ? "error" : "success"](`${losses ? "Losses" : "Profits"}: ${totalProfits}`);
          log[balance < currency.amount ? "error" : "success"](`Balance: ${balance}`);
        }

        /** CUSTOM LOGGING */
        console.formatify({
          resultMultiplier,
          resultPayout,
          nextBet,
          totalProfits,
          pause,
          hardStop,

        });
        /*******************/
          
        game.betQueue.shift();
        
      } catch(e) {
        console.error(e);
        log.error("Error! " + e.message);
      };

      // Trampoline the bet requests
      if (!hardStop && !pause) {
        if (typeof trampBet !== "undefined") {
          // config.rateLimit.value += 0.01;
          game.betQueue.push(setTimeout(trampBet, game.betQueue.length * (config.rateLimit.value * 1500)));
        }
      }
    });
    
  }/* END BET HANDLER */
};
