// Martingale
var staticBaseBet = 0.000015
var config = {
  baseBetTitle: { label: 'Set your Bet', type: 'title' },
  baseBet: { label: 'base bet', value: staticBaseBet, type: 'number' },
  payout: { label: 'payout', value: 1.5, type: 'number' },
  onStopTitle: { label: 'Configure Limit', type: 'title' },
  onStop: { 
    label: '', value: 'reset', type: 'radio',
    options: [
      { value: 'reset', label: 'Return to base bet' },
      { value: 'stop', label: 'stop after max bet' }
    ]
  },
  stop: { label: 'Limit: ', value: (function() {
    return ((config && config.baseBet.value) || staticBaseBet || currency.minAmount) * 3 || currency.minAmount;
  })(), type: 'number' },
  stopIfTitle: { label: 'Conditional Stop', type: 'title' },
  stopIf: { 
    label: '', value: 'stop', type: 'radio',
    options: [
      { value: 'ride', label: 'Don\'t stop' },
      { value: 'stop', label: 'stop after loss of below amount' }
    ]
  },
  stopIfValue: { label: 'Limit: ', value: (function() {
    return ((config && config.baseBet.value) || staticBaseBet || currency.minAmount) * 10 || currency.minAmount;
  })(), type: 'number' },
  onLoseTitle: { label: 'On Lose', type: 'title' },
  onLoss: { 
    label: '', value: 'increase', type: 'radio',
    options: [
      { value: 'reset', label: 'Return to base bet' },
      { value: 'increase', label: 'Increase bet by (loss multiplier)' }
    ]
  },
  lossMultiplier: { label: 'loss multiplier', value: 3, type: 'number' },
  onWinTitle: { label: 'On Win', type: 'title' },
  onWin: { 
    label: '', value: 'reset', type: 'radio',
    options: [
      { value: 'reset', label: 'Return to base bet' },
      { value: 'increase', label: 'Increase bet by (win multiplier)' }
    ]
  },
  winMultiplier: { label: 'win multiplier', value: 0, type: 'number' },
}

function main () {
  var currentBet = config.baseBet.value;
  var allLosses = 0;
  engine.on('GAME_STARTING', function () {
    engine.bet(currentBet, config.payout.value);
  })

  engine.on('GAME_ENDED', function () {
    var history = engine.getHistory()
    var lastGame = history[0]
    // If we wagered, it means we played
    if (!lastGame.wager) {
      return;
    }

    // we won..
    if (lastGame.cashedAt) {
      if (config.onWin.value === 'reset') {
        currentBet = config.baseBet.value;
      } else {
        currentBet *= config.winMultiplier.value;
      }
      log.success('We won, so next bet will be ' + currentBet + ' ' + currency.currencyName);
    } else {
      allLosses += lastGame.wager
      if (config.onLoss.value === 'reset') {
        currentBet = config.baseBet.value;
      } else {
        currentBet *= config.lossMultiplier.value;
      }
      log.error('We lost, so next bet will be ' + currentBet + ' ' + currency.currencyName);
    }

    if (currentBet > config.stop.value) {
      log.error('Was about to bet' + currentBet + 'which triggers the ' + config.onStop.value);
      if (config.onStop.value === 'reset') {
        currentBet = config.baseBet.value;
      } else {
        engine.stop();
      }
    }
    log.error('Loses: ' + allLosses);
    if (config.stopIf.value === 'stop' && allLosses >= config.stopIf.value) {
      log.error('Lost ' + allLosses + ', stopping the game.');
    }
  })
}