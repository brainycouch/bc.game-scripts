// DeclarativeWager.js

const DEBUG_BASE_BET_VALUE = 1.2e-7;
const DEBUG_BASE_LOSS_MULTIPLIER = 6.2;

/* GLOBALS */
let balance = currency.amount;
let history = [];
let config = {};

/* Utilities */
const printBet = game => (game.won ? log.success : log.error)(
  `Betting ${game.wager.toPrecision(9)} @ ${game.odds.toFixed(3)}x`
);

let initialFactorialBet;
const factorial = (bet, multiplier, losses) => {
  // bet = (typeof bet == "number")  ? bet : await bet;
  if (config.lossCount.value == losses) {
    initialFactorialBet = bet;
    log.info(`After 0 loss, next bet would be: ${bet}`);
  }
  let lossNum = config.lossCount.value - losses + 1;
  let lastBet = bet;
  bet += initialFactorialBet * Math.pow(multiplier, lossNum);
  log.info(`After ${lossNum.toPrecision(2)} loss, bet would be: ${bet.toPrecision(9)}`);
  if (bet > balance || (lossNum - 1) == config.lossCount.value) {
    log.info(`Max Bet set to ${(lossNum - 1).toPrecision(2)}x losses;\nNext bet would have been ${bet.toPrecision(9)}`);
    return lastBet;
  }
  return losses > 0 ? factorial(bet, multiplier, --losses) : bet;
};

const maxBetCalc = (bet = config.bet.value) => {
  const maxMultiplier = Math.pow(
    config.lossMultiplier.value,
    config.lossCount.value
  );
  return bet * maxMultiplier;
}

const baseBetCalc = (
  max = balance,
  multiplier = config.lossMultiplier.value,
  losses = config.lossCount.value
) => {
  // let x = currency.minAmount;
  // let newMax = max - (x * Math.pow(multiplier, losses));
  // max = (x * Math.pow(6.2, 6) + 
  //       (x * Math.pow(6.2, 5) + 
  //       (x * Math.pow(6.2, 4) + 
  //       (x * Math.pow(6.2, 3) + 
  //       (x * Math.pow(6.2, 2) + 
  //       (x * Math.pow(6.2, 1);
  // let newMax = max - ((max / config.lossCount.value) / (Math.pow(multiplier, losses)*max));
  let num = config.lossCount.value - losses + 2;
  log.info(`num: ${num}; max: ${max}`);
  let newMax = max / num;
  let maxLoss = factorial(newMax, multiplier, num);
  if (maxLoss > balance) {
    log.info(`Losses ${losses}: newMax: ${newMax}; maxLoss: ${maxLoss}`);
    return baseBetCalc(newMax, multiplier, --losses);
  } else {
    log.info(`Losses ${losses}: returning bet: ${max}; newMax: ${newMax}`);
    return max
  }
};

// CONFIGURATION

/** BETTING **/
config.bet = {
  label: 'bet',
  value: DEBUG_BASE_BET_VALUE,
  type: 'number'
};

/** PAYOUT **/
config.payoutTitle = { type: 'title', label: 'Payout' };
config.payout = {
  label: 'Odds (%): ',
  value: 1.2,
  type: 'number'
};

/** LOSS COUNT **/
config.lossCountTitle = { type: 'title', label: 'Loss Multiplier' };
config.lossCount = {
  label: 'Max Number of Losses: ',
  value: 6,
  type: 'number'
};

/** LOSS Multiplier **/
config.lossMultiplierTitle = { type: 'title', label: 'Loss Multiplier' };
config.lossMultiplier = {
  label: 'Multiply Bet on Loss by:: ',
  value: DEBUG_BASE_LOSS_MULTIPLIER,
  type: 'number'
};

/** PERFORM MinBetCalc */
// let setBet = async () => {
//   config.bet.value = await baseBetCalc();
// };


/** Max Bet **/
config.maxBetTitle = { type: 'title', label: 'Max Bet' };
config.maxBet = {
  label: 'Maximum Bet: ',
  value: maxBetCalc(),
  type: 'number'
};

config.maxLossesTitle = {
  type: 'title',
  label: 'Max Loss Potential - DO NOT EDIT'
};
config.maxLosses = {
  label: 'Maximum Losses: ',
  value: factorial(
    config.bet.value,
    config.lossMultiplier.value,
    config.lossCount.value
  ),
  type: 'number'
};

// setBet().then(async () => {
//   config.maxLosses.value = await factorial(
//     config.bet.value,
//     config.lossMultiplier.value,
//     config.lossCount.value
//   );
// });

let queue = [];

let Game = game => {
  let { won, payout, wager, odds, result } = game;
  history.push({ won, payout, wager, odds, result });
  return { won, payout, wager, odds, result };
}

let main = () => {

  game.won = true;
  game.payout = -1;
  game.wager = config.bet.value;
  game.odds = config.payout.value;
  game.result = 1;

  game.onBet = async () => {

    /* History */
    let lastGame = Game(game);
    if (!(lastGame.payout < 0)) {
      lastGame.won = lastGame.result > 0;
      lastGame.payout = lastGame.wager * (
        lastGame.won
          ? lastGame.odds
          : -1
      );

      balance += lastGame.payout;

      // log about it
      log[lastGame.won ? 'success' : 'error'](
        `${lastGame.won ? 'Won ' : 'Lost'}: ${lastGame.payout}; Total ${balance.toPrecision(9)}`
      );
    } else {
      lastGame.payout = 0;
    }

    // push onto queue
    history.push(lastGame);

    /* Risk Manipulation */
    // do stuff here to determine bet and odds
    game.wager = lastGame.won
      ? config.bet.value
      : lastGame.wager * config.lossMultiplier.value;


    /* PrintBet */
    printBet(game);

    if (game.wager > config.maxBet.value) {
      log.error(`STOPPED!! Was about to bet ${game.wager.toPrecision(9)}!`);
      game.stop();
      queue.forEach(que => {
        clearTimeout(que);
      });
      return;
    }

    /* Update Game */
    let nextGame = {
      ...game,
      id: `${(new Date()).getTime()}|${game.result}|${parseFloat(game.wager).toPrecision(9)}`,
      payout: 0,
      // odds: game.odds, // already set above
      // wager: game.wager, // already set above
      result: await game.bet(game.wager, game.odds)
    };

    queue.push(setTimeout(() => {
      let anotherGame = nextGame;
      !game.won && nextGame.won &&  anotherGame.bet(anotherGame.wager, anotherGame.odds);
    }, 250));

    // console.log("NEXT GAME:", nextGame);
    // console.log("LAST GAME:", lastGame);

    game = nextGame;
    delete nextGame;

  };

};