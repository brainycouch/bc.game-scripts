const DEBUG_BASE_BET_VALUE = 0.00005;
const DEBUG_BASE_LOSS_MULTIPLIER = 6.2;
const DEBUG_MAX_NUM_LOSSES = 3;
const DEBUG_BASE_PAYOUT = 1.78;
const DEBUG_MINIMUM_ODDS = 1.1;

/* GLOBALS */
let balance = currency.amount;
let history = [];
let config = {
  betTitle: {},
  bet: {},
  payoutTitle: {},
  payout: {},
  lossCountTitle: {},
  lossCount: {},
  lossMultiplierTitle: {},
  lossMultiplier: {},
  maxLossesTitle: {},
  maxLosses: {},
  maxBetTitle: {},
  maxBet: {}
};

Object.entries(config)
  .map(([k,], i) => config[k] = { order: i, label: '', value: '', type: '' });

let wagerForumla = ({wager, odds}, losses, lastLoss) => {
  // return losses * ((config.payout.value - game.odds + 1));
  //return (game.wager + losses + (((losses^game.odds)/game.odds)^(game.odds/(game.odds*4)))) * 4;
  // return game.wager + (losses * 2.5);

 // return (losses / game.odds) + (losses * ((config.lossMultiplier.value/game.odds) * (config.payout.value^2 - DEBUG_MINIMUM_ODDS*2)));
  // return (((game.wager^game.odds)/game.odds)^(game.odds/(((game.odds)*2)+game.odds)));
  // return ((game.wager^game.odds)/game.odds)^(game.odds/(config.payout.value - game.odds + 1));
  // return game.wager + (losses * (losses/game.wager));
  // return (((losses^game.odds)/game.odds)^(game.odds/(((game.odds-1)*2)+game.odds)));
  return (((odds - 1) * 100) * lastLoss) + losses;
};


/* Utilities */
let init = (async () => {
  const printBet = game => (game.won ? log.success : log.error)(
    `Betting ${game.wager.toPrecision(9)} @ ${game.odds.toFixed(3)}x`
  );

  let initialBet;
  const factorial = async (bet, multiplier, losses, update = true) => {
    // bet = (typeof bet == "number")  ? bet : await bet;
    if (config.lossCount.value == losses) {
      initialBet = bet;
      // log.info(`After 0 loss, next bet would be: ${bet.toPrecision(9)}`);
    }
    let lossNum = config.lossCount.value - losses + 1;
    let lastBet = bet;
    bet += initialBet * Math.pow(multiplier, lossNum);
    // log.info(`After ${lossNum.toPrecision(2)} loss, bet would be: ${bet.toPrecision(9)}`);
    if (update && (bet > balance || (lossNum - 1) == config.lossCount.value)) {
      let actualLosses = lossNum - 1;
      // config.lossCount.value = actualLosses;
      log.info(`Max Bet set to ${actualLosses.toPrecision(2)}x losses;\nNext bet would have been ${bet.toPrecision(9)}`);
      return lastBet;
    }
    return losses > 0 ? await factorial(bet, multiplier, --losses) : bet;
  };

  const maxBetCalc = (bet = config.bet.value) => {
    const maxMultiplier = Math.pow(
      config.lossMultiplier.value,
      config.lossCount.value
    );
    return bet * maxMultiplier;
  }

  const baseBetCalc = async (
    max = balance,
    multiplier = config.lossMultiplier.value,
    losses = config.lossCount.value,
    bet = config.bet.value
  ) => {
    // let x = currency.minAmount;
    // let newMax = max - (x * Math.pow(multiplier, losses));
    // max = (x * Math.pow(6.2, 6) + 
    //       (x * Math.pow(6.2, 5) + 
    //       (x * Math.pow(6.2, 4) + 
    //       (x * Math.pow(6.2, 3) + 
    //       (x * Math.pow(6.2, 2) + 
    //       (x * Math.pow(6.2, 1);
    // let newMax = max - ((max / config.lossCount.value) / (Math.pow(multiplier, losses)*max));
    let f = await factorial(bet, multiplier, losses - 1, false);
    let ret = bet;
    if (max > f) {
      log.info("min bet too low");
      do {
        if (bet < balance) {
          bet *= 2;
        } else {
          log.info('hit max bet...')
          ret = bet / 2;
          break;
        }
      } while ((f = await factorial(bet, multiplier, losses - 1, false)) > max)
    } else {
      log.info("min bet too high");
      do {
        log.info(`FACTORIAL: ${f}; bet: ${bet}`);
        if (bet < currency.minAmount) {
          log.info("hit min bet...");
          ret = currency.minAmount;
          break;
        } else {
          bet /= 2;
        }
      } while ((f = await factorial(bet, multiplier, losses - 1, false)) < max)
    }
    log.info(`min bet: ${ret}; f: ${f}`);
    return ret;
  };

  return {
    printBet,
    factorial,
    maxBetCalc,
    baseBetCalc
  };
});


// CONFIGURATION

/** BETTING **/
config.betTitle = { order: 1, type: 'title', label: 'Base Bet' };
config.bet = {
  order: 2,
  label: 'bet',
  value: DEBUG_BASE_BET_VALUE,
  type: 'number'
};

/** PAYOUT **/
config.payoutTitle = { order: 3, type: 'title', label: 'Payout (Odds)' };
config.payout = {
  order: 4,
  label: 'Odds (1.0102 - 99000): ',
  value: DEBUG_BASE_PAYOUT,
  type: 'number'
};

/** LOSS COUNT **/
config.lossCountTitle = { order: 5, type: 'title', label: 'Loss Count' };
config.lossCount = {
  order: 6,
  label: 'Max Number of Losses: ',
  value: DEBUG_MAX_NUM_LOSSES,
  type: 'number'
};

/** LOSS Multiplier **/
config.lossMultiplierTitle = { order: 7, type: 'title', label: 'Loss Multiplier' };
config.lossMultiplier = {
  order: 8,
  label: 'Multiply Bet on Loss by: ',
  value: DEBUG_BASE_LOSS_MULTIPLIER,
  type: 'number'
};

/** Max Bet **/
config.maxLossesTitle = { order: 11, type: 'title', label: 'Max Loss Potential - DO NOT EDIT' };
config.maxLosses = {
  order: 12,
  label: 'Maximum Losses: ',
  value: 'Check the log...',
  // value: await _.factorial(
  //     config.bet.value,
  //     config.lossMultiplier.value,
  //     config.lossCount.value
  // ),
  type: 'number'
};

config.maxBetTitle = { order: 9, type: 'title', label: 'Max Bet' };
config.maxBet = {
  order: 10,
  label: 'Maximum Bet: ',
  value: 3.1,
  type: 'number'
};

config = Object.entries(config)
  .sort(([, a], [, b]) => a.order - b.order)
  .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

let _ = {};
init().then(async __ => {
  _ = __;
  config.maxLosses.value = await _.factorial(
    config.bet.value,
    config.lossMultiplier.value,
    config.lossCount.value
  );
  log.info("- -");
  config.maxBet.value = _.maxBetCalc();
  log.info("-_-");
  config.bet.value = await _.baseBetCalc();
  log.info("-^-");
  Object.entries(config)
    // .filter(([,v]) => v.value && v.label)
    .forEach(([k, v]) => (log.info(v.label + (v.value ? v.value : ''), !v.value && log.info('-'))));
});

let queue = [];

let clearQueue = () => {
  queue.forEach(que => {
    clearTimeout(que);
  });
};

let main = async () => {

  game.won = true;
  game.payout = -1;
  game.wager = config.bet.value;
  game.odds = config.payout.value;
  game.result = 1;
  game.lossMultiplier = config.lossMultiplier.value;

  let losses = [];
  let wins = [];
  let oddsMax = config.payout.value;
  let oddsMin = DEBUG_MINIMUM_ODDS;
  let oddsLossAdjustment = -0.1;
  let oddsWinAdjustment = 0.1;

  let oddsAdjustment = (config.payout.value - oddsMin) / (config.lossCount.value - 1);
  oddsLossAdjustment = oddsAdjustment * -1;
  oddsWinAdjustment = oddsAdjustment;

  game.onBet = async () => {

    /* History */
    let lastGame = game;
    if (!(lastGame.payout < 0 && lastGame.result > 0)) {
      lastGame.won = lastGame.result > 0;
      lastGame.payout = lastGame.wager * (
        lastGame.won
          ? lastGame.odds
          : -1
      );

      balance += lastGame.payout;

      // log about it
      log[lastGame.won ? 'success' : 'error'](
        `${lastGame.won ? 'Won' : 'Lost'}: ${lastGame.payout.toPrecision(9)}; Total ${balance.toFixed(8)}`
      );
    } else {
      lastGame.payout = 0;
    }

    if (game.won && game.payout > 0) {
      wins.push(lastGame);
      losses = [];
      game.odds = config.payout.value;
    } else if (!game.won) {
      losses.push(lastGame);
      wins = [];
    }
    let lossRallyCount = losses.length;
    let lossRallyPayout = losses.reduce(((r, v) => (r + v.payout)), 0);
    if (lossRallyCount > 3) {
      log.info(`Losses at ${lossRallyCount}, ${lossRallyPayout.toPrecision(9)} @ ${game.odds}`);
    }
    // push onto queue
    history.push(lastGame);

    /* Risk Manipulation */
    // odds
    if (lastGame.won) {
      if (game.odds < config.payout.value) {
        game.odds += oddsAdjustment
      } else {
        game.odds = config.payout.value;
      }
    } else {
      if (game.odds > (oddsMin + oddsAdjustment)) {
        game.odds -= oddsAdjustment
      } else {
        game.odds = oddsMin;
      }
    }
    // loss multiplier
    game.lossMultiplier += lastGame.won
      ? (((game.lossMultiplier - 5.5) / 2) * -1)
      : ((7 - game.lossMultiplier) / 2);

    // wager
    if (lastGame.won) {
      game.wager = config.bet.value;
    } else {
      // game.wager += ((lossRallyPayout * -1)) * (game.odds + (oddsLossAdjustment * (lossRallyCount + 1)));
      // game.wager = (((game.wager - lossRallyPayout) * game.odds) + game.wager) * (game.odds - 1);
      // game.wager += ((lossRallyPayout * -1) * game.odds);
      // game.wager = (game.wager - lossRallyPayout) * (game.wager - 1) * -1;
      /// game.wager += ((game.wager - lossRallyPayout) * (game.wager+((lossRallyCount/config.lossCount.value))/2))
      // game.wager += (game.wager + (lossRallyPayout * -1) * (game.odds - 1 - oddsAdjustment));
      let posLosses = lossRallyPayout * -1;
      let posLastLoss = lastGame.payout * -1;
      // game.wager = (lastGame.payout * -1) + (posLosses * (game.odds - oddsAdjustment));
      //// game.wager += posLosses + (posLosses * game.odds); // decent, less than 4 losses
      // game.wager += posLosses + (posLosses * (game.odds + (oddsAdjustment * (lossRallyCount))));
      // v--- best yet
      // game.wager = (posLastLoss / game.odds) + (posLastLoss * config.lossMultiplier.value);
      // v--- better yet
      //game.wager = (posLastLoss / (game.odds + (lossRallyCount >= 5 ? (oddsAdjustment * (lossRallyCount - 4 * 1.5)): 0))) + (posLastLoss * ((config.lossMultiplier.value/game.odds) * 1.2));
      // game.wager = posLosses + (posLastLoss * ((config.payout.value - game.odds) * ((config.payout.value * game.odds) + 1)));
      game.wager = wagerForumla(game, posLosses, posLastLoss);
    }

    if (game.wager < currency.minAmount) {
      game.wager = currency.minAmount;
      log.info("Reset Bet to Minimum");
    }

    /* PrintBet */
    _.printBet(game);

    if (game.wager > config.maxBet.value) {
      log.error(`STOPPED!! Was about to bet ${game.wager.toFixed(8)}!`);
      game.stop();
      clearQueue();
      return;
    }

    (async () => {
      let catchBet = game;
      if (!game.won && !!lastGame.won) {
        let doCatchBet = () => {
          // if(!game.won && history.length > 1 && history[history.length - 2].won) {
            debugger;
            let result = catchBet.bet(catchBet.wager, catchBet.odds);
            catchBet.won = result > 0;
            if (catchBet.won && lastGame.won) {
              doCatchBet();
            } else {
              clearQueue();
            }
          // }
        };
        queue.push(setTimeout(doCatchBet, 500));
      }
    })();

    /* Update Game */
    game = {
      ...game,
      id: `${(new Date()).getTime()}|${game.result}|${parseFloat(game.wager).toPrecision(9)}`,
      payout: 0,
      // odds: game.odds, // already set above
      // wager: game.wager, // already set above
      result: await game.bet(game.wager, game.odds)
    };


    // console.log("NEXT GAME:", nextGame);
    // console.log("LAST GAME:", lastGame);

  };
};