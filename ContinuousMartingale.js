// Continuous Martingale
;;;
let decimals = val => {
  let s = val.toString().split('e-');
  return `${s.length > 1 ? '0.' : ''}${'0'.repeat(s[1]-1)}${s[0]}`;
};
var game = {};
let orgLog = log;
let doLogging = (type, message) => {
  console.info(message);
  orgLog[type](message);
};
/* override `log` object */
log = {
  success: msg => doLogging('success', msg),
  info:    msg => doLogging('info', msg),
  error:   msg => doLogging('error', msg),
  debug:   console.debug
};
var staticBaseBet = 0.0000015;
var minAmountDecimals = decimals(currency.minAmount).length - (currency.minAmount.toString().match('e') ? 1 : 0); // only 1 cause we want additional decimal
// log.error("minAmount: " + currency.minAmount + "; Digits: " + minAmountDecimals);
var config = {
  // BETTING
  baseBetTitle: { label: 'Set Your Bet', type: 'title' },
  baseBet: { label: 'Bet:    ', value: staticBaseBet, type: 'number' },
  payout:  { label: 'Payout: ', value: 1.25, type: 'number' },
  // LOSS BET RESET
  onLimitTitle: { label: 'Maximum Bet', type: 'title' },
  onLimit: { 
    label: '', value: 'reset', type: 'radio',
    options: [
      { value: 'reset', label: 'Reset When Over Max Bet' },
      { value: 'stop', label: 'Stop After Max Bet' }
    ]
  },
  limit: { label: 'Maximum: ', value: (function() {
    // return 0.00006;
    return (((config && config.baseBet.value) || staticBaseBet || currency.minAmount) * 3 || currency.minAmount).toFixed(minAmountDecimals);
  })(), type: 'number' },
  // CANCEL(STOP) GAME
  stopIfTitle: { label: 'Maximum Losses', type: 'title' },
  stopIf: { 
    label: '', value: 'stop', type: 'radio',
    options: [
      { value: 'ride', label: 'Don\'t Stop' },
      { value: 'stop', label: 'Stop at Maximum' }
    ]
  },
  stopIfValue: { label: 'Maximum: ', value: (function() {
    // return 0.00018;
    let calcd = (((config && config.baseBet.value) || staticBaseBet || currency.minAmount) * 10 || currency.minAmount).toFixed(minAmountDecimals);
    if (calcd > 0.00018) {
      return 0.00018;
    }
    return calcd;
  })(), type: 'number' },
  // WHEN YOU LOSE
  onLoseTitle: { label: 'When You Lose', type: 'title' },
  onLoss: { 
    label: '', value: 'resetMultiplier', type: 'radio',
    options: [
      { value: 'reset', label: 'Return to base bet' },
      { value: 'increase', label: 'Increase by Bet Multiplier' },
      { value: 'resetMultiplier', label: 'Reset to Base Multiplier' }
    ]
  },
  lossMultiplier:      { label: 'Bet Multiplier:  ', value: 2, type: 'number' },
  lossResetMultiplier: { label: 'Base Multiplier: ', value: 2, type: 'number' },
  // LOSS PREVENTION (PAUSE)
  pauseTitle: { label: 'Loss Prevention', type: 'title' },
  pause: { 
    label: '', value: 'pause', type: 'radio',
    options: [
      { value: 'pause', label: 'Pause After # of Losses' },
      { value: 'ride', label: 'Ignore Losses' }
    ]
  },
  lossCount:  { label: '# of Losses: ', value: 2, type: 'number' },
  pauseCount: { label: '# of Pauses: ', value: 2, type: 'number' },
  // WHEN YOU WIN
  onWinTitle: { label: 'When You Win', type: 'title' },
  onWin: { 
    label: '', value: 'increase', type: 'radio',
    options: [
      { value: 'reset', label: 'Return to Starting Bet' },
      { value: 'increase', label: 'Increase Bet by Multiplier' }
    ]
  },
  winMultiplier:    { label: 'Multiplier: ', value: 1.2, type: 'number' },
  winResetMin:      { label: 'Min Wins:   ', value: 1, type: 'number' },
  winResetMax:      { label: 'Max Wins:   ', value: 2, type: 'number' },
}

function main () {
  try {

    log.debug(config);

    game = {
      nextBet:    config.baseBet.value,
      profits:    0,
      winCount:   0,
      lossCount:  0,
      pauseCount: 0,
    };

    // log.error(`Initial Loss Count ${game.lossCount}; Pause Count: ${game.pauseCount}`);

    let updateProfit = lastGame => {
      let profitUpdate = parseFloat(game.nextBet);
      profitUpdate *= lastGame.cashedAt ? parseFloat(config.payout.value) : -1;
      game.profits += profitUpdate;
      log[
        game.profits < 0 
          ? "error" 
          : "success"
      ](
        ( game.profits < 0
          ? 'Losses'
          : 'Profits'
        ) + ': '
        + game.profits.toFixed(minAmountDecimals)
      );
      return game.profits;
    };

    engine.on('GAME_STARTING', function () {
      log.debug(game);
      log.info(' -------------- ');
      log.info(`[${Date.now()}]`);
      try {
        let doBet = async () => {
          let nextBet = game.nextBet.toFixed(minAmountDecimals);
          await engine.bet(nextBet, config.payout.value);
          if (config.onWin.value == 'increase') {
            log.info(`Actually betting ${nextBet} at payout of ${config.payout.value}x`);
          }
          log.info('...');
        };
        if (config.pause.value == 'pause') {
          if (game.lossCount == config.lossCount.value) {
            log.error('Maximum Sequential Losses');
            if (game.pauseCount == config.pauseCount.value) {
              log.info('Resuming Betting');
              game.pauseCount = 0;
              game.lossCount = config.lossCount.value - 1;
              game.nextBet = config.baseBet.value;
            } else {
              game.pauseCount++;
              let pausesLeft = config.pauseCount.value - game.pauseCount + 1;
              log.error(`Skipping the next ${pausesLeft} bets.`);
              return false;
            }
          }
        }
        doBet();
      } catch(e) {
        try {
          log.error("Error: " + e.message);
          return false;
        } catch(err) {
          log.error("THERE WAS AN ERROR WITH THE ERROR");
          return false;
        }
      }
    });

    engine.on('GAME_ENDED', function () {
      var history = engine.getHistory()
      var lastGame = history[0]
      // If we wagered, it means we played
      if (!lastGame.wager) {
        return;
      }

      updateProfit(lastGame);

      /** HANDLE RESULT **/

      if (lastGame.cashedAt) {
        /* WIN */
        if (config.onWin.value === 'reset') {
          game.nextBet = config.baseBet.value;
        } else {
          if (game.winCount >= config.winResetMin.value 
          &&  game.winCount < config.winResetMax.value
          ) {
            game.nextBet *= config.winMultiplier.value;
          } else {
            game.nextBet = config.baseBet.value;
          }
        }
        game.lossCount = 0;
        game.winCount++;
        log.success(" ---- WINNER ---- ");
      } else {
        /* LOSE */
        if (config.onLoss.value === 'reset') {
          game.nextBet = config.baseBet.value;
        } else if (config.onLoss.value === 'multiplier') {
          game.nextBet *= config.lossMultiplier.value;
        } else {
          game.nextBet = config.baseBet.value * config.lossResetMultiplier.value;
        }
        game.winCount = 0;
        game.lossCount++;
        log.error(' ---- LOSER! ---- ');
      }

      /** LARGE BET **/

      if (game.nextBet > config.limit.value) {
        log.error('Was about to bet ' + game.nextBet + ' ' + currency.currencyName);
        if (config.onLimit.value === 'reset') {
          game.nextBet = config.baseBet.value;
          log.error('Resetting to base bet');
        } else {
          log.error('Bet too much. Quitting game.');
          engine.stop();
        }
      }

      /** PROFITS **/

      if (config.stopIf.value === 'stop' 
      && game.profits < 0 
      && game.profits <= Math.abs(config.stopIf.value)) {
        log.error('Lost ' + Math.abs(game.profits) + '. Quitting the game.');
      }
    });
  } catch(e) {
    log.error("ERROR: " + e);
  }

}