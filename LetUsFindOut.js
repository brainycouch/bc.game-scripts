const DEBUG_BASE_BET_VALUE = 1.2e-6;
const DEBUG_BASE_LOSS_MULTIPLIER = 5.5;
const DEBUG_MAX_NUM_LOSSES = 8;

/* GLOBALS */
let balance = currency.amount;
let history = [];
let config = {
  betTitle: {},
  bet: {},
  payoutTitle: {},
  payout: {},
  lossCountTitle: {},
  lossCount: {},
  lossMultiplierTitle: {},
  lossMultiplier: {},
  maxLossesTitle: {},
  maxLosses: {},
  maxBetTitle: {},
  maxBet: {}
};

const _ = async () => {

  /* Utilities */
  const printBet = game => log.info(
    `Betting ${game.wager.toPrecision(9)} @ ${game.odds.toFixed(3)}x`
  );

  let initialBet;
  const factorial = async (bet, multiplier, losses) => {
    // bet = (typeof bet == "number")  ? bet : await bet;
    if (config.lossCount.value == losses) {
      initialBet = bet;
      log.info(`After 0 loss, next bet would be: ${bet.toPrecision(9)}`);
    }
    let lossNum = config.lossCount.value - losses + 1;
    let lastBet = bet;
    bet += initialBet * Math.pow(multiplier, lossNum);
    log.info(`After ${lossNum.toPrecision(2)} loss, bet would be: ${bet.toPrecision(9)}`);
    if (bet > balance || (lossNum - 1) == config.lossCount.value) {
      let actualLosses = lossNum - 1;
      config.lossCount.value = actualLosses;
      log.info(`Max Bet set to ${actualLosses.toPrecision(2)}x losses;\nNext bet would have been ${bet.toPrecision(9)}`);
      return lastBet;
    }
    return losses > 0 ? await factorial(bet, multiplier, --losses) : bet;
  };

  const maxBetCalc = (bet = config.bet.value) => {
    const maxMultiplier = Math.pow(
      config.lossMultiplier.value,
      config.lossCount.value
    );
    return bet * maxMultiplier;
  }

  const baseBetCalc = async (
    max = balance,
    multiplier = config.lossMultiplier.value,
    losses = config.lossCount.value
  ) => {
    // let x = currency.minAmount;
    // let newMax = max - (x * Math.pow(multiplier, losses));
    // max = (x * Math.pow(6.2, 6) + 
    //       (x * Math.pow(6.2, 5) + 
    //       (x * Math.pow(6.2, 4) + 
    //       (x * Math.pow(6.2, 3) + 
    //       (x * Math.pow(6.2, 2) + 
    //       (x * Math.pow(6.2, 1);
    // let newMax = max - ((max / config.lossCount.value) / (Math.pow(multiplier, losses)*max));
    let num = config.lossCount.value - losses + 2;
    log.info(`num: ${num.toPrecision(2)}; max: ${max.toPrecision(9)}`);
    let newMax = max / num;
    let maxLoss = await factorial(newMax, multiplier, num);
    if (maxLoss > balance) {
      log.info(`Losses ${losses.toPrecision(2)}: newMax: ${newMax.toPrecision(9)}; maxLoss: ${maxLoss.toPrecision(9)}`);
      return baseBetCalc(newMax, multiplier, --losses);
    } else {
      log.info(`Losses ${losses.toPrecision(2)}: returning bet: ${max.toPrecision(9)}; newMax: ${newMax.toPrecision(9)}`);
      return max;
    }
  };

  let sortConfig = _config => Object.entries(_config)
    .sort(([,a], [,b]) => a.order - b.order)
    .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

  return {
    printBet,
    factorial,
    maxBetCalc,
    baseBetCalc,
    sortConfig
  };
};


// CONFIGURATION

/** BETTING **/
config.betTitle = { order: 1, type: 'title', label: 'Base Bet' };
config.bet = {
  order: 2,
  label: 'bet',
  value: DEBUG_BASE_BET_VALUE,
  type: 'number'
};

/** PAYOUT **/
config.payoutTitle = { order: 3, type: 'title', label: 'Payout (Odds)' };
config.payout = {
  order: 4,
  label: 'Odds (1.0102 - 99000): ',
  value: 1.25,
  type: 'number'
};

/** LOSS COUNT **/
config.lossCountTitle = { order: 5, type: 'title', label: 'Loss Multiplier' };
config.lossCount = {
  order: 6,
  label: 'Max Number of Losses: ',
  value: DEBUG_MAX_NUM_LOSSES,
  type: 'number'
};

/** LOSS Multiplier **/
config.lossMultiplierTitle = { order: 7, type: 'title', label: 'Loss Multiplier' };
config.lossMultiplier = {
  order: 8,
  label: 'Multiply Bet on Loss by: ',
  value: DEBUG_BASE_LOSS_MULTIPLIER,
  type: 'number'
};

/** PERFORM MinBetCalc */
let potentialLoss = await factorial(
  config.bet.value,
  config.lossMultiplier.value,
  config.lossCount.value
);
let setBet = await (async () => {
   config.bet.value = await baseBetCalc();
});


/** Max Bet **/
config.maxLossesTitle = { order: 11, type: 'title', label: 'Max Loss Potential - DO NOT EDIT' };
config.maxLosses = {
  order: 12,
  label: 'Maximum Losses: ',
  value: factorial(
    config.bet.value,
    config.lossMultiplier.value,
    config.lossCount.value
  ),
  type: 'number'
};

config.maxBetTitle = { order: 9, type: 'title', label: 'Max Bet' };
config.maxBet = {
  order: 10,
  label: 'Maximum Bet: ',
  value: maxBetCalc(),
  type: 'number'
};


// setBet().then(async () => {
 //   config.maxLosses.value = await factorial(
//     config.bet.value,
//     config.lossMultiplier.value,
//     config.lossCount.value
//   );
// });

config = sortConfig(config);

let queue = [];

let main = () => {

  game.won = true;
  game.payout = -1;
  game.wager = config.bet.value;
  game.odds = config.payout.value;
  game.result = 1;
  game.lossMultiplier = config.lossMultiplier.value;

  game.onBet = async () => {

    /* History */
    let lastGame = game;
    if (!(lastGame.payout < 0)) {
      lastGame.won = lastGame.result > 0;
      lastGame.payout = lastGame.wager * (
        lastGame.won
          ? lastGame.odds
          : -1
      );

      balance += lastGame.payout;

      // log about it
      log[lastGame.won ? 'success' : 'error'](
        `${lastGame.won ? 'Won' : 'Lost'}: ${lastGame.payout.toPrecision(9)}; Total ${balance.toFixed(8)}`
      );
    } else {
      lastGame.payout = 0;
    }

    // push onto queue
    history.push(lastGame);


    /* Risk Manipulation */
    // do stuff here to determine bet and odds
    game.lossMultiplier += lastGame.won
      ? (((game.lossMultiplier - 5.5) / 2) * -1)
      : ((7 - game.lossMultiplier) / 2);
    game.wager = lastGame.won
      ? config.bet.value
      : lastGame.wager * game.lossMultiplier;
    game.odds += lastGame.won ? 0.1 : -0.1;
    if (game.odds < 1.0102) {
      game.odds = 1.0102;
    } else if (game.odds > 2) {
      game.odds = config.payout.value;
    }

    /* PrintBet */
    // printBet(game);

    if (game.wager > config.maxBet.value) {
      log.error(`STOPPED!! Was about to bet ${game.wager.toFixed(8)}!`);
      game.stop();
      queue.forEach(que => {
        clearTimeout(que);
      });
      return;
    }

    /* Update Game */
    let nextGame = {
      ...game,
      id: `${(new Date()).getTime()}|${game.result}|${parseFloat(game.wager).toPrecision(9)}`,
      payout: 0,
      // odds: game.odds, // already set above
      // wager: game.wager, // already set above
      result: await game.bet(game.wager, game.odds)
    };

    queue.push(setTimeout(() => {
      let anotherGame = nextGame;
      game.won && history[history.length-1].won && queue.length < 1 && anotherGame.bet(anotherGame.wager, anotherGame.odds);
    }, 500));

    // console.log("NEXT GAME:", nextGame);
    // console.log("LAST GAME:", lastGame);

    game = nextGame;
    delete nextGame;

  };

};